## Tic Tac Toe

A simple implementation of Tic Tac Toe played via CLI and written in Go.

### Test

```
go test ./...
```

### Build & Run

```
cd cmd/ttt
go build .
./ttt
```

> Describe some changes that would need to be made to the code in order to transform it into an online, asynchronous application, supporting many concurrent games. You can either write some code, or just describe what needs to be done. We don't expect you to write a fully working program, e.g. if you want to use a database, just write a "Database" interface and code against that.

Defining and exposing an API for the game would allow us to support online, async and concurrent games. Two probable RESTful endpoints being:

```
GET /api/v1/games/{id}
POST /api/v1/games/{id}/moves
```

The GET would expose some serialised (e.g. JSON/proto) representation of the game state to be interpreted by a client.
The POST would allow a player to submit a move that, if accepted as valid, would update the game state.

Some form of storage would be necessary, either a traditional database (e.g. Postgres, Mongo) or by simply storing the game's serialised representation in object storage (e.g. S3, Minio).

Some mechanism to prevent unwanted/unauthorised interaction by people other than the players would be needed. This could take the form of _real_ identification and authentication involving a sign-up flow. Alternatively a more simple but less secure mechanism of opaque, long URLs known only to the player's clients could be used.

> Write about how you would take the code/changes in step 2 and deploy it into the cloud. The requirements are that it needs to support a global user base, but you should be mindful that the solution also needs to be cost-effective.

The app should be, depending on our platform (e.g. K8s, ECS), containerised to provide a consistent and predictable deployment environment. 

Instrumentation and monitoring of the game server's runtime metrics and endpoints will provide confidence in production. As examples: HTTP response times and status codes, processing time for the game's core logic, open file descriptors, Goroutines, GC counts and times. Dashboards and alerting should be configured based on these metrics such that we're automatically informed when things are going wrong.

We should run multiple instances fronted by load balancers in various availability zones and regions to provide redundancy as well as better performance to local users of a given region. Autoscaling based on load will allow us to support a global user base while keeping capacity and therefore cost closer to a reasonable minimum. GeoDNS can be leveraged to route traffic to a user's closest datacentre.

Any cache or database powering the application should also be instrumented, monitored and replicated.