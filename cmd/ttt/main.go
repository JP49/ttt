package main

import (
	"os"

	"gitlab.com/jp49/ttt/internal/board"
	"gitlab.com/jp49/ttt/internal/game"
	"gitlab.com/jp49/ttt/internal/ui"
)

func main() {
	console := ui.New(os.Stdin, os.Stdout)
	b := board.New()
	game.New(console, b).StartGame()
}