package board

const BLANK = " "

// Board represents the grid of possible positions
type Board struct {
	positions []string
}

// New constructs a new board
func New() *Board {
	return &Board{[]string{BLANK, BLANK, BLANK, BLANK, BLANK, BLANK, BLANK, BLANK, BLANK}}
}

// Positions returns a copy of the boards positions
func (b Board) Positions() []string {
	return b.positions
}

// SetMark sets a mark on the board, returning true if the move was valid or false if it was not.
func (b *Board) SetMark(pos int, mark string) bool {
	if pos >= 0 && pos < len(b.positions) && b.positions[pos] == BLANK {
		b.positions[pos] = mark
		return true
	}
	return false
}

// GetWinner checks the board for a winning combination of moves, returning the players mark and a flag
// indicating if there has been a winner.
func (b *Board) GetWinner() (*string, bool) {
	for _, solution := range solutions() {
		if allSpacesMatch(b, solution) {
			return &b.positions[solution[0]], true
		}
	}
	return nil, false
}

// IsGameOver checks whether the game has ended, either via victory or running out of blank positions.
func (b *Board) IsGameOver() bool {
	return b.solutionFound() || b.isFull()
}

func solutions() [][]int {
	return [][]int{{0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {0, 3, 6}, {1, 4, 7}, {2, 5, 8}, {0, 4, 8}, {2, 4, 6}}
}

func (b *Board) solutionFound() bool {
	solutionFound := false
	for _, set := range solutions() {
		solutionFound = solutionFound || allSpacesMatch(b, set)
	}
	return solutionFound
}

func (b *Board) isFull() bool {
	for _, position := range b.positions {
		if position == BLANK {
			return false
		}
	}
	return true
}

func allSpacesMatch(board *Board, pos []int) bool {
	position := board.positions[pos[0]]
	allMatch := position != BLANK
	for _, i := range pos {
		allMatch = allMatch && board.positions[i] == position
	}
	return allMatch
}
