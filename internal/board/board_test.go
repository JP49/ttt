package board

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInvalidMoveIsRejected(t *testing.T) {
	board := New()
	assert.False(t, board.SetMark(-1, "X"), "expected negative int to be invalid")
	assert.False(t, board.SetMark(10, "O"), "expected OOB int to be invalid")
}

func TestWinningSolutionIsDetected(t *testing.T) {
	board := New()
	board.SetMark(0, "X")
	board.SetMark(1, "X")
	board.SetMark(2, "X")
	assert.True(t, board.IsGameOver(), "expected horizontal matches to be detected")
	winner, isWon := board.GetWinner()
	assert.True(t, isWon, "expected horizontal matches to be detected")
	assert.Equal(t, "X", *winner, "expected X to be detected as winner")

	board = New()
	board.SetMark(0, "X")
	board.SetMark(4, "X")
	board.SetMark(8, "X")
	assert.True(t, board.IsGameOver(), "expected diagonal matches to be detected")
	winner, isWon = board.GetWinner()
	assert.True(t, isWon, "expected diagonal matches to be detected")
	assert.Equal(t, "X", *winner, "expected X to be detected as winner")

	board = New()
	board.SetMark(0, "X")
	board.SetMark(3, "X")
	board.SetMark(6, "X")
	assert.True(t, board.IsGameOver(), "expected vertical matches to be detected")
	winner, isWon = board.GetWinner()
	assert.True(t, isWon, "expected vertical matches to be detected")
	assert.Equal(t, "X", *winner, "expected X to be detected as winner")
}
