package game

import (
	"gitlab.com/jp49/ttt/internal/board"
	"gitlab.com/jp49/ttt/internal/ui"
)

// TicTacToe represents an instance of a game of TicTacToe
type TicTacToe struct {
	gameUI    ui.UI
	gameBoard *board.Board
	turns     int
}

// New constructs a new instance of TicTacToe using the provided board and UI.
func New(u ui.UI, b *board.Board) *TicTacToe {
	return &TicTacToe{
		gameBoard: b,
		gameUI:    u,
	}
}

// StartGame starts the game loop, blocking until it completes.
func (t *TicTacToe) StartGame() {
	t.gameUI.Start()

	for !t.gameBoard.IsGameOver() {
		t.gameUI.ShowBoard(t.gameBoard)
		move := t.gameUI.GetMove()
		if !t.gameBoard.SetMark(move, markForTurn(t.turns)) {
			t.gameUI.InvalidMove()
			continue
		}
		t.turns++
	}

	t.gameUI.ShowBoard(t.gameBoard)
	t.gameUI.End(t.gameBoard)
}

func markForTurn(turn int) string {
	if turn%2 == 0 {
		return "X"
	}
	return "O"
}
