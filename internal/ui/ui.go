package ui

import (
	"fmt"
	"io"
	"strconv"
	"strings"

	"gitlab.com/jp49/ttt/internal/board"
)

const NEWLINE = '\n'

type UI interface {
	Start()
	End(*board.Board)
	ShowBoard(*board.Board)
	InvalidMove()
	GetMove() int
}

type ConsoleUI struct {
	in  io.Reader
	out io.Writer
}

func New(in io.Reader, out io.Writer) UI {
	return &ConsoleUI{out: out, in: in}
}

// Start prints the starting banner
func (u *ConsoleUI) Start() {
	WriteLine(u.out, "Tic Tac Toe")
}

// ShowBoard prints a text representation of a TicTacToe board
func (u *ConsoleUI) ShowBoard(b *board.Board) {
	for i := 0; i < 3; i++ {
		WriteLine(u.out, strings.Join(b.Positions()[i*3:i*3+3], "|"))
	}
}

// GetMove prompts the player for a move
func (u *ConsoleUI) GetMove() int {
	WriteLine(u.out, "Select a position (0-8):")
	line := ReadLine(u.in)
	move, err := strconv.Atoi(line)
	for err != nil {
		WriteLine(u.out, "Invalid move, try again")
		line := ReadLine(u.in)
		move, err = strconv.Atoi(line)
	}
	return move
}

func (u *ConsoleUI) InvalidMove() {
	WriteLine(u.out, "Invalid move, try again")
}

func (u *ConsoleUI) End(b *board.Board) {
	player, isWinner := b.GetWinner()
	if !isWinner {
		WriteLine(u.out, "Game was a draw")
		return
	}
	WriteLine(u.out, fmt.Sprintf("%s won the game!", *player))
}

func WriteLine(w io.Writer, s string) {
	_, err := io.WriteString(w, s+"\n")
	if err != nil {
		panic(err)
	}
}

func ReadLine(reader io.Reader) string {
	var buffer = make([]byte, 1)
	var line string
	for {
		_, err := reader.Read(buffer)
		if buffer[0] == NEWLINE || err == io.EOF {
			break
		}
		line += string(buffer[0])
	}
	return line
}
