package ui

import (
	"bytes"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInvalidInputIsRequired(t *testing.T) {
	input := "helloworld\n1231a412123\n\n1"
	in := bytes.NewReader([]byte(input))
	ui := New(in, os.Stdout)
	assert.Equal(t, 1, ui.GetMove(), "expected invalid input to be discarded")
}
